import React, { useContext } from 'react';
import { MyContext } from '../context/myContext';

const Input = props => {
    const myContext = useContext(MyContext);
    
    return (
        <div className='InputContainer'>
            <input
                className='Input' 
                type='text' 
                value={myContext.input} 
                onChange={(event) => myContext.setInput(event.target.value)}/>
            <button 
                onClick={() => myContext.addFunction()} 
                disabled={myContext.input.length ? false : true }>Add Folder</button>
        </div>
    );
}

export default Input;