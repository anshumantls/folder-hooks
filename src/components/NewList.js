import React, { useContext } from 'react';
import { MyContext } from '../context/myContext';

//images
import Folder from '../assets/folder.png';

const NewList = props => {
    const myContext = useContext(MyContext)
    const folderList = myContext.state;
    const select = myContext.editSelect;
    let myFolders = '';
    const _setMyFolders = (list) => {
        myFolders = list.map(each => {
            if(each.children.length) {
                return  <ul key={each.id}>
                            <li>
                                <p 
                                    className={each.id === myContext.select ? 'Active' : null} 
                                    onClick={() => select(each.id)}>
                                        <img alt='icon' src={Folder}/>
                                        {each.folderName}
                                </p>
                            </li>
                            <div >{_setMyFolders(each.children)}</div>
                        </ul>
            } else {
                return  <ul key={each.id}>
                            <p 
                                className={each.id === myContext.select ? 'Active' : null} 
                                onClick={() => select(each.id)}>
                                    <img alt='icon' src={Folder}/>
                                    {each.folderName}
                            </p>
                        </ul>
            }
        })
        return myFolders;
    }
    return (
        <div>
            {_setMyFolders(folderList)}
        </div>
    )   
}

export default NewList;