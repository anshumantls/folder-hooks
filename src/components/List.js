import React, { useContext } from 'react';
import { MyContext } from '../context/myContext';

//images
import Folder from '../assets/folder.png';

const List = props => {
    const myContext = useContext(MyContext);
    const folderList = myContext.state;
    return (
        <div>
            {folderList.map(each => {
                return  <div key={each.id}>
                            <p 
                                className={each.id === myContext.select ? 'Active Parent' : 'Parent'} 
                                onClick={() => {myContext.editSelect(each.id)}}>
                                    <img alt='folder' src={Folder}/>{each.folderName}
                            </p>
                            {each.children.map(each => {
                                return  <div key={each.id}>
                                            <p 
                                                className={each.id === myContext.select ? 'Active Child' : 'Child'} 
                                                onClick={() => {myContext.editSelect(each.id)}}>
                                                    <img alt='folder' src={Folder}/>{each.folderName}
                                            </p>
                                            {each.children.map(each => {
                                                return  <div key={each.id}>
                                                            <p 
                                                                className={each.id === myContext.select ? 'Active Grandchild' : 'Grandchild'} 
                                                                onClick={() => {myContext.editSelect(each.id)}}>
                                                                    <img alt='folder' src={Folder}/>{each.folderName}
                                                            </p>
                                                            {each.children.map(each => {
                                                                return <div>lol</div>
                                                            })}
                                                        </div>
                                            })}
                                        </div>
                            })}
                        </div>
            })}
        </div>
    );
}

export default List;