import React, { useContext } from 'react';
import './App.css';

//components
// import List from './components/List';
import NewList from './components/NewList';
import Input from './components/Input';
import { MyContext } from './context/myContext';

const App = () => {
  const myContext = useContext(MyContext);
  return (
    <div>
      <div className="App">
        <div>
          <Input/>
          <ul style={{listStyle:'none'}}>
            <NewList/>
          </ul>
        </div>
      </div>
      <div onClick={() => {myContext.editSelect('')}}style={{height:'230px',border:'1px solid white'}}></div>
    </div>
  );
}

export default App;
