import React, { useState } from 'react';

export const MyContext = React.createContext({
    state: [],
    changeState: () => {},
    input: '',
    changeInput: () => {},
    addFunction : () => {},
    setSelect: () => {}
});

const MyContextProvider = props => {
    const [tree , setTree] = useState([
        {
          folderName: '1',
          id:'0000',
          children:[{
            folderName:'1.1',
            id:'00001',
            children:[{
              folderName:'1.1.1',
              id:'000011',
              children:[]
            }]
          }]
        },
        {
          folderName: '2',
          id:'0001',
          children:[{
            folderName:'2.1',
            id:'00002',
            children:[{
              folderName:'2.1.1',
              id:'000021',
              children:[]
            }]
          }]
        }
      ]);
    const [input, setInput] = useState('');
    const [select, setSelect] = useState('');

    const _changeMySelection = (input) => {
      console.log('changing');
      setSelect(input);
      console.log(select,'selected');
      return;
    }

    const _findFolderAndAddData = (arr,id,folder) => {
      const newTree = [...arr];
      newTree.forEach(each => {
        if(each.id === id) {
          each.children.push(folder)
          setInput('');
        } else {
          _findFolderAndAddData(each.children,id,folder)
        }
      })
      // for (let i=0; i<newTree.length; i++) {
      //   if(newTree[i].id === id){
      //     newTree[i].children.push(folder)
      //     return;
      //   } else {
      //     _findFolderAndAddData(newTree[i].children,id,folder);
      //   }
      // }
      setTree(newTree);
    }

    const _addFunction = () => {
      const newTree = [...tree]
      const newFolder = {
        folderName : input,
        id: Date.now(),
        children:[]
      }
      if(!select) {
        newTree.push(newFolder);
        setTree(newTree);
        setInput(''); 
        return;
      } else {
        _findFolderAndAddData(newTree,select,newFolder);
      }
    }

    return (
        <MyContext.Provider value={{
            state: [...tree], changeState: setTree,
            input:input, setInput:setInput ,
            addFunction: _addFunction,
            editSelect:_changeMySelection, select:select}}>
            {props.children}
        </MyContext.Provider>
    );
}

export default MyContextProvider;
